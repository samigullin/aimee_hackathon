from telegram import *
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from AimeeApiSDK import AimeeApiSDK
from tinydb import TinyDB
import time

updater = Updater(token='570424499:AAGUvT9EIzpFMZAaxX18q5T-rygcUZJaNDw')
dispatcher = updater.dispatcher
sdk = AimeeApiSDK("51.144.183.234:4560", 0.4)
sdk.set_config({'min_answers': 1, 'min_distinct_operators': 1})
# TODO: change coefficient

db_contexts = TinyDB('contexts.json')
session_context = "меню"
print("Bot is alive")
print(session_context)


def textMessage(bot, update):
	global session_context

	message = update.message
	temp_text = str(message.text).lower()
	answer, context = sdk.get_answer(temp_text, session_context)

	if answer is not None:
		bot.send_message(chat_id=message.chat_id, text=answer)
		if session_context != context:
			session_context = context
		if context == "информация":
			kasko_info(bot, update)
			osago_info(bot, update)
			session_context = "меню"
		elif context == "оформление":
			kasko_form(bot, update)
			osago_form(bot, update)
		elif context == "техосмотр":
			location(bot, update)
			session_context = "меню"
		elif context == "адреса":
			location(bot, update)
			session_context = "меню"
		print(session_context)
	else:
		bot.send_message(chat_id=message.chat_id, text='Ответ не распознан. Пожалуйста, перефразируйте.')




def startCommand(bot, update):
	global session_context
	chat_id = update.message.chat_id
	if session_context == 'меню':
		answer = "Привет я бот-консультант в автосервисе (Здесь может быть ваш автосервис)"
		bot.send_message(chat_id=chat_id, text=answer)
		time.sleep(1)
	bot.send_message(chat_id=chat_id, text='Вы можете меня спросить практически о всем, что косается автосервиса')
	time.sleep(1)
	bot.send_message(chat_id=chat_id, text='`-`*КАСКО / ОСАГО*', parse_mode=ParseMode.MARKDOWN)
	bot.send_message(chat_id=chat_id, text='`-`*Покупка автомобиля*', parse_mode=ParseMode.MARKDOWN)
	bot.send_message(chat_id=chat_id, text='`-`*Адреса магазинов*', parse_mode=ParseMode.MARKDOWN)
	bot.send_message(chat_id=chat_id, text='`-`*Техосмотр*', parse_mode=ParseMode.MARKDOWN)


'''
/insert
question
answer
context
'''


def insertCommand(bot, update):
	text = update.message.text
	text_blocks = str(text).split("\n")
	if len(text_blocks) >= 4:
		sdk.send_data(text_blocks[1], text_blocks[2], 'id1')
		db_contexts.insert({'answer': text_blocks[2], 'context': text_blocks[3]})


def suggestionCommand(bot, update):
	text = update.message.text
	text_blocks = str(text).split("\n")
	if len(text_blocks) >= 2:
		bot.send_message(chat_id=update.message.chat_id, text=str(sdk.get_suggestions(text_blocks[1])))


def kasko_info(bot, update):
	keyboard = [[InlineKeyboardButton('Инфо', url='https://goo.gl/bbihY9')]]
	reply_markup = InlineKeyboardMarkup(keyboard)
	update.message.reply_text('Информация о КАСКО', reply_markup=reply_markup)


def osago_info(bot, update):
	keyboard = [[InlineKeyboardButton('Инфо', url='https://goo.gl/ngmzCH')]]
	reply_markup = InlineKeyboardMarkup(keyboard)
	update.message.reply_text('Информация об ОСАГО', reply_markup=reply_markup)


def kasko_form(bot, update):
	keyboard = [[InlineKeyboardButton('Форма', url='https://goo.gl/forms/XAQP8XrHb48X7hOv2')]]
	reply_markup = InlineKeyboardMarkup(keyboard)
	update.message.reply_text(
		'Форма для предварительного оформления КАСКО, оставшаяся часть документов будет офрмлена в салоне',
		reply_markup=reply_markup)


def osago_form(bot, update):
	keyboard = [[InlineKeyboardButton('Форма', url='https://goo.gl/forms/iJcFeID1ynYCXow83')]]
	reply_markup = InlineKeyboardMarkup(keyboard)
	update.message.reply_text('Форма для оформления ОСАГО', reply_markup=reply_markup)


def location(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Вот несколько наших автосалонов")
	bot.send_message(chat_id=update.message.chat_id, text="Московская ул., 20, Казань, Респ. Татарстан, 420111")
	bot.send_location(chat_id=update.message.chat_id, latitude=55.7906596, longitude=49.1052328, live_period=80)
	bot.send_message(chat_id=update.message.chat_id, text="Казань, Респ. Татарстан, 420061")
	bot.send_location(chat_id=update.message.chat_id, latitude=55.7967087, longitude=49.1912644, live_period=80)
	bot.send_message(chat_id=update.message.chat_id, text="ул. Габдуллы Тукая, 115 корпус 3, Казань, Респ. Татарстан")
	bot.send_location(chat_id=update.message.chat_id, latitude=55.766002, longitude=49.1269089, live_period=80)


start_command_handler = CommandHandler('start', startCommand)
insert_command_handler = CommandHandler('insert', insertCommand)
suggestion_command_handler = CommandHandler('suggestion', suggestionCommand)

text_message_handler = MessageHandler(Filters.text, textMessage)

dispatcher.add_handler(text_message_handler)
dispatcher.add_handler(start_command_handler)
dispatcher.add_handler(insert_command_handler)
dispatcher.add_handler(suggestion_command_handler)

updater.start_polling(clean=True)
